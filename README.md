# godot-bits

Small samples for learning Godot game engine.

# Samples
All samples are placed under `samples` folder.

So far there are following samples:

* move sprite to mouse with rotation;
* runtime PathFollow2D building and moving sprite across it;

# License
Code is under `The Unlicense`, public domain.