extends Node2D

# Some hardcoded points
var point_a = Vector2(100, 100)
var point_b = Vector2(500, 100)
var point_c = Vector2(300, 300)

var path_follow
var icon_ref
var speed = 100

func _ready():
	var path = Path2D.new()
	var curve = Curve2D.new()
	curve.add_point(point_a)
	curve.add_point(point_b)
	curve.add_point(point_c)
	curve.add_point(point_a)
	path.set_curve(curve)
	
	call_deferred("add_child", path)
	path_follow = PathFollow2D.new()
	path.call_deferred("add_child", path_follow)
	icon_ref = $icon # this is short form of get_node("icon")
	icon_ref.position = point_a

func _draw():
	draw_circle(point_a, 9, Color(.5, .5, .0))
	draw_circle(point_b, 9, Color(.5, .0, .5))
	draw_circle(point_c, 9, Color(.0, .5, .5))

func _process(delta):
	path_follow.set_offset(path_follow.get_offset() + speed * delta)
	icon_ref.position = path_follow.position
	icon_ref.rotation = path_follow.rotation
	icon_ref.rotate(deg2rad(270))
