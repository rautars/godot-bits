extends Node2D

# variable defined with 'export' means that value can be modified from editor
export (int) var speed = 200

var target = Vector2()
var velocity = Vector2()

var icon_ref

func _ready():
	icon_ref = $icon
	target = icon_ref.position

func _input(event):
	if event.is_action_pressed('my_mouse_left_click'):
		target = get_global_mouse_position()
		
		# rotate godot icon in way that robot mouth will face to red point
		icon_ref.look_at(target)
		icon_ref.rotate(deg2rad(270))

# draw here is just visual demonstration
func _draw():
	draw_circle(target, 3, Color(1, 0, 0))

func _process(delta):
	var icon_pos = icon_ref.position
	velocity = (target - icon_pos).normalized() * speed
	if (target - icon_pos).length() > 5:
		icon_ref.position += velocity * delta
	update()
